<?php

namespace App;

use App\User;
use App\Period;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'course_name','season','class_id','period_id','lecturer_id'
    ];

    public function lecturers()
    {
        return $this->belongsTo('App\User','lecturer_id','id');
    }

    public function periods()
    {
        return $this->belongsTo('App\Period','period_id','period_id');
    }
    
}
