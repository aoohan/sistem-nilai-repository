<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;


class DosenController extends Controller
{
    
    public function index()
    {
        $dosens = \App\User::where('role_id',2)->get();
        $unverifieds =\App\User::where('role_id',5)->get();
        return view('dosen.index',compact('dosens','unverifieds'));
    }

    public function verifikasi(Request $request, $id)
    {
        
        $target = \App\User::find($id);
        User::find($id)
        ->update([
            'name' => $target->name,
            'email' => $target->email,
            'role_id' => 2
        ]); 

        return redirect('/dosen');
    }

    public function destroy(Request $request, $id)
    {
        
        User::find($id)->delete();
        /*
        $target = \App\User::where('id',$id)->get(); SALAH CODE
        $target -> delete();*/

        return redirect('/dosen');
        
    }

}
