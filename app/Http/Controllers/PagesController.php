<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nilai;

class PagesController extends Controller
{
    

    public function nilai()
    {
        $nim = null;
        $found = true;
        return view('nilai',compact('found','nim',));
    }

    public function cariNilai(Request $request)
    {
        $hasil = Nilai::where('nim',$request->nim)->get();
        $found = true;
        $nim = $request->nim;

            return view('result',compact('hasil','found','nim'));
        
    }
}
