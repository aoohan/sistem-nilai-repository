<?php

namespace App\Http\Controllers;

use App\Course;
use App\Period;
use App\Kelas;
use App\User;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\NilaiImport;
use App\Nilai;
use Illuminate\Support\Facades\Storage;

class coursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = \App\Course::all();   
        

        return view('courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periods = \App\Period::all();
        $classes = \App\Kelas::all();
        $lecturers = \App\User::where('role_id',2)->get();

        return view('courses.create',compact('periods','classes','lecturers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Course::create([
            'course_name' => $request->course_name,
            'season' => $request->season,
            'class_id' => $request->class_id,
            'period_id' => $request->period_id,
            'lecturer_id' => $request->lecturer_id
        ]);

        return redirect('/course');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $nilais = \App\Nilai::where('course_id',$id)->get();
        $target = \App\Course::where('course_id',$id)->first();

        return view('courses.show',compact('nilais','target','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $periods = \App\Period::all();
        $classes = \App\Kelas::all();
        $lecturers = \App\User::where('role_id',2)->get();
        $target = \App\Course::where('course_id',$id)->first();
        return view('courses.edit',compact('periods','classes','lecturers','target'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        Course::where('course_id',$id)
        ->update([
            'course_name' => $request->course_name,
            'lecturer_id' => $request->lecturer_id
        ]);

        return redirect('/course');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Course::where('course_id',$id)->delete();
        return redirect('/course');

    }

    public function import(Request $request)
    {
        

        //Catch Excel File
        $file = $request->file('file');

        //Make a unique file name
        $nama_file = rand().$file->getClientOriginalName();
        
        
        //Upload ke folder file_nilai di dalam folder public
        $file->storeAs('/file_nilai/',$nama_file);

        //Import Data
        Excel::import(new NilaiImport,$file,$request->id);

        //notifikasi dengan session
        
        $courses = \App\Course::all();
        return redirect('/course')->with('courses');

    }

    public function tambah(Request $request)
    {
        $id = $request->id;

        Nilai::create([
            'nim' => $request->nim,
            'mahasiswa_name' => $request->nama,
            'course_id' => $id,
            'jenis_ujian' => $request->jenis_ujian,
            'nilai_angka' => $request->nilai_angka
        ]);

        return redirect('course/show/'.$id);
    }
}
