<?php

namespace App\Imports;

use App\Nilai;
use Maatwebsite\Excel\Concerns\ToModel;

class NilaiImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Nilai([
            'nim' => $row[0],
            'mahasiswa_name' => $row[1],
            'course_id' => $row[2],
            'nilai_angka' => $row[3],
            'jenis_ujian' => $row[4]
        ]);
    }
}
