<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;

class Nilai extends Model
{
    
    protected $fillable = [
        'nim','mahasiswa_name','jenis_ujian','course_id','nilai_angka','nilai_huruf'
    ];

    public function courses()
    {
        return $this->belongsTo('\App\Course','course_id','course_id');
    }

}
