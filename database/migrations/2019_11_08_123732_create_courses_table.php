<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('course_id');
            $table->timestamps();
            $table->string('course_name');
            $table->string('season');

            $table->unsignedBigInteger('class_id');
            $table->unsignedBigInteger('period_id');
            $table->unsignedBigInteger('lecturer_id');

            $table->foreign('class_id')->references('class_id')->on('classes');
            $table->foreign('period_id')->references('period_id')->on('periods');
            $table->foreign('lecturer_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
