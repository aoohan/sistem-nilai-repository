<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/nilai','PagesController@nilai');
Route::post('/nilai','PagesController@cariNilai');

//Route si Super Admin
Route::group(['middleware' => ['auth', 'checkRole:1']], function () {
    Route::get('/course', 'coursesController@index');
    Route::get('/course/create', 'coursesController@create');
    Route::post('/course/import_nilai','coursesController@import');
    Route::post('/course/tambah_nilai','coursesController@tambah');
    Route::post('/course', 'coursesController@store');
    Route::get('/course/show/{course}', 'coursesController@show');
    Route::get('/course/{course}/edit', 'coursesController@edit');
    Route::patch('/course/{course}/update', 'coursesController@update');
    Route::patch('/course/{course}/destroy', 'coursesController@destroy');

    Route::get('/period', 'periodsController@index');
    Route::get('/period/create', 'periodsController@create');
    Route::post('/period', 'periodsController@store');

    Route::get('/class/create', 'classesController@create');
    Route::post('/class', 'classesController@store');

    Route::get('/dosen', 'DosenController@index');
    Route::patch('/dosen/{user}/verifikasi', 'DosenController@verifikasi');
    Route::patch('/dosen/{user}/destroy', 'DosenController@destroy');
});

// Route si Dosen
Route::group(['middleware' => ['auth', 'checkRole:1,2']], function () {
    Route::get('/course', 'coursesController@index');
    Route::get('/course/show/{course}', 'coursesController@show');
    Route::post('/course/import_nilai','coursesController@import');
    Route::post('/course/tambah_nilai','coursesController@tambah');
});
