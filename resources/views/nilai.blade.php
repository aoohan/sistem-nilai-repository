@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body shadow-sm">
                        <form action="" method="post" class="">
                            @csrf
                            <div class="input-group col-md-12 form-inline">
                                <div class="input-group-prepend">
                                </div>
                                <input type="text" class="form-control mb-2 mr-sm-2" name="nim" id="nim" placeholder="Masukkan Nim">
                                <button type="submit" class="btn btn-success  mb-2 ">Cari</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
@endsection