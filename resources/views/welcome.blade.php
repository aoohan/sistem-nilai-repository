@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-2 justify-content-center">
            <div class="col-md-10 text-center">
                <h1>Welcome to eQuilibrium</h1>    
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10 text-center">
                    <a href="/nilai" class="btn btn-success">Cari Nilai</a>
            </div>
        </div>
    </div>
@endsection