@extends('layouts.app')

@section('content')

@if ($hasil->count() == 0)
    <div class="row my-3 justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-danger" role="alert">
                Maaf! Nim Anda Tidak Ditemukan
            </div>
        </div>
    </div>
@else
<div class="row my-3 justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    Hasil Pencarian untuk nim {{ $nim }}
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="table-head">
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Pengampu</th>
                            <th>Jenis Ujian</th>
                            <th>Nilai</th>
                        </thead>
                        <tbody>
                            @foreach ($hasil as $item)
                            <tr>
                                <td>{{ $item->mahasiswa_name }}</td>
                                <td>{{ $item->courses->course_name }}</td>
                                <td>{{ $item->courses->lecturers->name }}</td>
                                <td>{{ $item->jenis_ujian }}</td>
                                <td>{{ $item->nilai_angka }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row justify-content-center">
    <div class="col-md-8">
        <a href="/nilai"><small>< Kembali</small></a>
    </div>
</div>
@endsection