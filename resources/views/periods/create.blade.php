@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-3 justify-content-center">
            <div class="col-md-8">
                <h4>Tambah Periode Baru</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="/period"><small>< kembali</small></a>
            </div>
        </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <form action="/period" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="period_name">Periode</label>
                                        <input type="text" name="period_name" id="period_name" placeholder="Nama Periode" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success float-right">Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection