@extends('layouts.app')

@section('content')
    
    <div class="container">
            <div class="row justify-content-center">
                    <div class="col-md-10">
                        <a href="/course"><small>< Kembali</small></a>
                    </div>
                </div>
        <div class="row mt-3 justify-content-center">
            <div class="col-md-8">
                <h4>Periode Akademik</h4>
            </div>
            <div class="col-md-2">
                <a href="/period/create" class="btn btn-success float-right shadow-sm">Tambah</a>
            </div>
        </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-10">
                @if( $periods->count()==null )
                    <div class="card">
                        <div class="card-body text-center">
                            <b>Tidak Ada Periode Akademik yang Tersedia</b>
                        </div>
                    </div> 
                @else
                    <div class="card  shadow-sm">
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-head table-darl">
                                    <th>No</th>
                                    <th>Periode</th>
                                </thead>
                                <tbody>
                                    @foreach ( $periods as $period)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $period->period_name }}</td>
                                    </tr>                                            
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="row mt-3 justify-content-center">
                <div class="col-md-8">
                    <h4>Kelas Pararel</h4>
                </div>
                <div class="col-md-2">
                    <a href="/class/create" class="btn btn-success float-right">Tambah</a>
                </div>
            </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-10">
                @if ( $classes->count() == null )
                    <div class="card shadow-sm">
                        <div class="card-body text-center">
                            <b>Tidah Ada Data Kelas yang Tersedia</b>
                        </div>
                    </div>                    
                @else
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-head">
                                    <th>Kelas</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ( $classes as $class)
                                        <tr>
                                            <td>
                                                {{ $class->class_name }}
                                            </td>
                                            <td>
                                                <form action="/class" method="post">
                                                @method('patch')
                                                    <a href="#" class="btn btn-success btn-sm">Edit</a>
                                                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>                                       
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection