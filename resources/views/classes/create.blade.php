@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mt-3 justify-content-center">
            <div class="col-md-8">
                <h4>Tambah Kelas Baru</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="/period"><small>< Kembali</small></a>
            </div>
        </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <form action="/class" method="post">
                        @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="class_name">Abjad Kelas</label>
                                        <input type="text" name="class_name" id="class_name" class="form-control" placeholder="Masukkan Abjad Kelas (huruf kapital)">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="period_id">Periode Akademik</label>
                                        <select name="period_id" id="period_id" class="form-control">
                                            @foreach ( $periods as $period)
                                                <option value="{{ $period->period_id }}">{{ $period->period_name }}</option>                                                
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success float-right">Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection