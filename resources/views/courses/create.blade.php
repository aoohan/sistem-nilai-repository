@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h4>Tambah Mata Kuliah Baru</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="/course"><small>< Kembali </small></a>
            </div>
        </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-8 ">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <form action="/course" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="course_name">Nama Mata Kuliah</label>
                                        <input type="text" name="course_name" id="course_name" class="form-control" placeholder="Masukkan Nama Mata Kuliah">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="period_id">Periode</label>
                                        <select name="period_id" id="period_id" class="form-control">
                                            @foreach ( $periods as $period)
                                                <option value="{{ $period->period_id }}">{{ $period->period_name }}</option>                                                
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="season">Musim</label>
                                        <select name="season" id="season" class="form-control">
                                                <option value="Ganjil">Ganjil</option>     
                                                <option value="Genap">Genap</option>                                     
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <label for="lecturer_id">Dosen Pengampu</label>
                                        <select name="lecturer_id" id="lecturer_id" class="form-control">
                                                <option value="">- Belum Tersedia -</option>
                                            @foreach ( $lecturers as $lecturer )
                                                <option value="{{ $lecturer->id }}">{{ $lecturer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="class_id">Kelas</label>
                                        <select name="class_id" id="class_id" class="form-control">
                                            @foreach ( $classes as $class )
                                                <option value="{{ $class->class_id }}"> {{ $class->class_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success float-right">Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection