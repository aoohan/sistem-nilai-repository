@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Sunting Mata Kuliah Baru</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <a href="/course"><small>< Kembali </small></a>
            </div>
        </div>
        <div class="row my-3 justify-content-center">
            <div class="col-md-6 ">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <form action="/course/{{ $target->course_id }}/update" method="post">
                            @csrf
                            @method('patch')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="course_name">Nama Mata Kuliah</label>
                                        <input type="text" name="course_name" id="course_name" class="form-control" placeholder="Masukkan Nama Mata Kuliah" value="{{ $target->course_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="lecturer_id">Dosen Pengampu</label>
                                        <select name="lecturer_id" id="lecturer_id" class="form-control">
                                                <option value="">- Belum Tersedia -</option>
                                            @foreach ( $lecturers as $lecturer )
                                                <option value="{{ $lecturer->id }}">{{ $lecturer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="float-right">
                                        <a href="/course" class="btn btn-danger">batal</a>
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection