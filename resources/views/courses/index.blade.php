@extends('layouts.app')


@section('content')
    <div class="container my-3">
        <div class="row">
            <div class="col-md-8">
                <h4>Mata Kuliah FTI-UII</h4>
            </div>
            @if(Auth::user()->role_id==1)
            <div class="col-md-4">
                <div class="float-right">
                    <a href="/period/" class="btn btn-primary">Periode</a>
                    <a href="/course/create" class="btn btn-success">Tambah</a>
                </div>
            </div>
            @endif
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Mata kuliah Aktif
                    </div>
                    @if ($courses->count()<1)
                        <div class="card-body text-center">
                            <b> Tidak Ada Data untuk Ditampilkan </b>
                         </div>
                    @else
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-head table-dark">
                                    <th>No</th>
                                    <th>Mata Kuliah</th>
                                    <th>Periode</th>
                                    <th>Musim</th>
                                    <th>Pengampu</th>
                                    @if(Auth::user()->role_id==1)
                                    <th>Pengaturan</th>
                                    @endif
                                </thead>
                                <tbody>
                                    @if(Auth::user()->role_id == 1)
                                    @foreach ( $courses as $course )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="/course/show/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                                            <td>{{ $course->periods->period_name }}</td>
                                            <td>{{ $course->season }}</td>
                                            <td>
                                                @if ( $course->lecturer_id == null )
                                                    <a href="/course/{{ $course->course_id }}/edit">Tambahkan</a>
                                                @else
                                                    {{ $course->lecturers->name }}
                                                @endif
                                            </td>
                                            <td>
                                                    <a href="/course/{{ $course->course_id }}/edit" class="btn btn-success btn-sm">Ubah</a>
                                                <form action="/course/{{ $course->course_id }}/destroy" method="post" class="d-inline">
                                                    @csrf
                                                    @method('patch')
                                                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @elseif(Auth::user()->role_id == 2)
                                    @foreach ( Auth::user()->courses as $course )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="/course/show/{{ $course->course_id }}">{{ $course->course_name }}</a></td>
                                            <td>{{ $course->periods->period_name }}</td>
                                            <td>{{ $course->season }}</td>
                                            <td>
                                                @if ( $course->lecturer_id == null )
                                                    <a href="/course/{{ $course->course_id }}/edit">Tambahkan</a>
                                                @else
                                                    {{ $course->lecturers->name }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection