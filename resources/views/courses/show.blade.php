@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-3 justify-content-center">
            
                <div class="col-md-5">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            Detail Mata Kuliah
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Kode Mata Kuliah</td>
                                        <td> : </td>
                                        <td> {{ $target->course_id }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Mata Kuliah</td>
                                        <td> : </td>
                                        <td> {{ $target->course_name }} </td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td> : </td>
                                        <td> {{ $target->periods->period_name }}  </td>
                                    </tr>
                                    <tr>
                                        <td>Pengampu</td>
                                        <td> : </td>
                                        <td> 
                                            @if ( $target->lecturer_id == null)
                                                <a href="/course/{{ $target->course_id }}/edit">Tambahkan</a>
                                            @else
                                                {{ $target->lecturers->name }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Masa</td>
                                        <td> : </td>
                                        <td>  {{ $target->season }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            Nilai Ujian 
                        </div>
                        <div class="card-body">
                            
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#import_nilai">
                                        Unggah File Nilai (Excel)
                                    </button> 
                                
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah_manual">
                                        Tambah Nilai Manual
                                    </button> 
                    
                                <!-- Import Nilai -->
                                <div class="modal fade" id="import_nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form action="/course/import_nilai" method="post" enctype="multipart/form-data">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Import Nilai</h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                        <div class="alert alert-warning" role="alert">
                                                        Format Tabel: Nim, Nama, ID Mata Kuliah, Nilai Angka, Jenis Ujian <br>
                                                        ID Mata Kuliah : {{$target->course_id}}
                                                        </div>

                                                        {{ csrf_field() }}
                                                        
                                                        <div class="form-group">
                                                            
                                                            <input type="file" name="file" id="file" class="">
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Unggah</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <!-- Tambahkan Nilai Manual -->
                                <div class="modal fade" id="tambah_manual" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form action="/course/tambah_nilai" method="post">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4>Tambah Nilai Manual</h4>
                                                </div>
                                                <div class="modal-body">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $target->course_id }}">
                                                    <div class="form-group">
                                                        <label for="nim">Nim</label>
                                                        <input type="text" name="nim" id="nim" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama">Nama</label>
                                                        <input type="text" name="nama" id="nama" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jenis_ujian">Jenis Ujian</label>
                                                        <select name="jenis_ujian" id="jenis_ujian" class="form-control">
                                                            <option value="UTS">UTS</option>
                                                            <option value="UAS">UAS</option>
                                                            <option value="UAS">REMEDIASI</option>
                                                            <option value="UAS">NILAI AKHIR</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nilai_angka">Nilai</label>
                                                        <input type="text" name="nilai_angka" id="nilai_angka" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                     
                    </div>
            
                </div>

        <div class="row my-3 justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Nilai Mahasiswa
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead class="table-header">
                                <th>No</th>
                                <th>Nim</th>
                                <th>Nama</th>
                                <th>Jenis Ujian</th>
                                <th>Nilai</th>
                            </thead>
                            <tbody>
                                @foreach ($nilais as $nilai)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $nilai->nim }}</td>
                                        <td>{{ $nilai->mahasiswa_name }}</td>
                                        <td>{{ $nilai->jenis_ujian }}</td>
                                        <td>{{ $nilai->nilai_angka }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <a href="/course"> < Kembali </a>
            </div>
        </div>
    </div>
@endsection