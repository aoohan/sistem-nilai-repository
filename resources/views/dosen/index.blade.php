@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-3 justify-content-center">
            <div class="col-md-10">
                <h4>Dosen Informatika UII</h4>
            </div>
        </div>
        <div class="row mb-3 justify-content-center">
            <div class="col-md-10">
                @if ( $dosens->count()===0)
                    <div class="card">
                        <div class="card-body text-center">
                            <b>Tidak Ada Data Dosen Aktif Tersedia</b>
                        </div>
                    </div>                    
                @else 
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-head">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Mata Kuliah</th>
                                </thead>
                                <tbody>
                                    @foreach ( $dosens as $dosen)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $dosen->name }}</td>
                                            <td>{{ $dosen->email }}</td>
                                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#modal{{$dosen->id}}">Lihat Semua</button></td>

                                            <!-- Modal Course Dosen -->
                                            <div class="modal fade" id="modal{{$dosen->id}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$dosen->id}}Title" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="modal{{$dosen->id}}Title">Mata Kuliah {{$dosen->name}}</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                      @foreach ($dosen->courses as $course)
                                                        <p>{{$loop->iteration}}. {{$course->course_name}}</p>
                                                      @endforeach
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        

        <div class="row mb-3 justify-content-center">
            <div class="col-md-10">
                @if ( $unverifieds->count()!==0)
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-head">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ( $unverifieds as $unv )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $unv->name }}</td>
                                            <td>{{ $unv->email }}</td>
                                            <td>
                                                <form action="dosen/{{ $unv->id }}/verifikasi" method="post" class="d-inline">
                                                    @csrf
                                                    @method('patch')
                                                    <button type="submit" class="btn btn-success btn-sm">verifikasi</button>
                                                </form>
                                                <form action="dosen/{{ $unv->id}}/destroy" method="post" class="d-inline">
                                                    @csrf
                                                    @method('patch')
                                                    <button type="submit" class="btn btn-danger btn-sm">Tolak</button>
                                                </form>
                                            </td>
                                        </tr>                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                @endif
            </div>
        </div>
    </div>
@endsection